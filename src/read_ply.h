#ifndef READ_PLY_H
#define READ_PLY_H

//== INCLUDES =================================================================

#include <vcg/complex/complex.h>
#include <vcg/simplex/face/topology.h>

#include <vcg/complex/algorithms/update/bounding.h>
#include <vcg/complex/algorithms/update/color.h>
#include <vcg/complex/algorithms/update/flag.h>
#include <vcg/complex/algorithms/update/normal.h>
#include <vcg/complex/algorithms/update/position.h>
#include <vcg/complex/algorithms/update/quality.h>
#include <vcg/complex/algorithms/update/selection.h>
#include <vcg/complex/algorithms/update/topology.h>

#include <vcg/complex/all_types.h>
#include <wrap/io_trimesh/import.h>
#include <wrap/io_trimesh/import_ply.h>
#include <wrap/ply/plylib.h>

#include <surface_mesh/surface_mesh.h>

//== VCG MESH DEFINITION =======================================================

class VcgEdge;
class VcgFace;
class VcgVertex;

class VcgUsedTypes: public vcg::UsedTypes < vcg::Use<VcgVertex>::AsVertexType,
        vcg::Use<VcgEdge   >::AsEdgeType,
        vcg::Use<VcgFace  >::AsFaceType >{};

class VcgVertex: public vcg::Vertex<VcgUsedTypes,
                    vcg::vertex::Coord3f, vcg::vertex::Color4b, vcg::vertex::BitFlags, vcg::vertex::Normal3f> {};

class VcgFace: public vcg::Face<VcgUsedTypes,
                vcg::face::VertexRef, vcg::vertex::Normal3f> {};

class VcgEdge : public vcg::Edge<VcgUsedTypes,
        vcg::edge::BitFlags,          /*  4b */
        vcg::edge::EVAdj,
        vcg::edge::EEAdj
>{};

class VcgMesh: public vcg::tri::TriMesh< std::vector<VcgVertex>, std::vector<VcgFace> > {};


//MODULE FUNCTIONS ==============================================================

bool readPLY(surface_mesh::Surface_mesh& mesh, const std::string& filename);

#endif // READ_PLY_H

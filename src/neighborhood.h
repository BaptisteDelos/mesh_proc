

#ifndef TD2_H
#define TD2_H

#include <surface_mesh/surface_mesh.h>
#include <stack>

// Returns the list of all vertices connected to v within a distance 'dist'
std::vector<surface_mesh::Surface_mesh::Vertex> select_neighbors(const surface_mesh::Surface_mesh& mesh, surface_mesh::Surface_mesh::Vertex v, float dist);

template<typename Func>
void foreach_neighbors(const surface_mesh::Surface_mesh& mesh, surface_mesh::Surface_mesh::Vertex v, float dist, const Func &f)
{
  f(v);

  Eigen::Vector3f q = mesh.position(v);
  std::vector<bool> visited(mesh.n_vertices(), false);
  visited[v.idx()] = true;
  std::stack<surface_mesh::Surface_mesh::Vertex> stack;
  stack.push(v);
  while(!stack.empty())
  {
    surface_mesh::Surface_mesh::Vertex vj = stack.top();
    stack.pop();

    // pour tous les voisins vi de vj
    surface_mesh::Surface_mesh::Halfedge h = mesh.halfedge(vj), h0 = h;
    do
    {
      surface_mesh::Surface_mesh::Vertex vi = mesh.to_vertex(h);
      if(!visited[vi.idx()]) // si on ne l'a pas déjà rencontré
      {
        if( (mesh.position(vi)-q).norm() < dist ) // si on n'est pas sortie de la boule
        {
          f(vi);
          stack.push(vi);
        }
      }
      visited[vi.idx()] = true;
      h = mesh.next_halfedge(mesh.opposite_halfedge(h));
    } while (h!=h0);
  }
}

#endif // TD2_H

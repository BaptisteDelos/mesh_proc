﻿#include "viewer.h"
#include "camera.h"
#include "SOIL2.h"

using namespace Eigen;
using namespace surface_mesh;

Viewer::Viewer() : _currentShader{ObjectShaderProgram::BLINN}, _currentScreenShader{ScreenShaderProgram::NEUTRAL}
{
}

Viewer::~Viewer()
{
    delete _screen;
    delete _gui;

    glDeleteVertexArrays(1, &_quadVAO);
}

////////////////////////////////////////////////////////////////////////////////
// GL stuff

// initialize OpenGL context
void Viewer::init(GLFWwindow* window, int w, int h, int argc, char **argv){
    _winWidth = w;
    _winHeight = h;

    std::cout << "Display:" << std::endl;
    std::cout << "  r:    reload shaders" << std::endl;
    std::cout << "  w:    enable/disable wireframe" << std::endl;
    std::cout << "  t:    enable/disable texturing" << std::endl;
    std::cout << "  g:    enable/disable cold-warm shading" << std::endl;
    std::cout << "  s:    enable/disable display of selected vertices" << std::endl;
    std::cout << "Camera:" << std::endl;
    std::cout << "  mouse left+drag:      camera rotation" << std::endl;
    std::cout << "**************************************************" << std::endl << std::endl;

    // set the background color, i.e., the color used
    // to fill the screen when calling glClear(GL_COLOR_BUFFER_BIT)
    glClearColor(0.24f,0.3f,0.3f,1.f);

    glEnable(GL_DEPTH_TEST);
    glBlendFunc(GL_ONE, GL_ONE);

    loadProgram(_currentShader);

    _texid = SOIL_load_OGL_texture(DATA_DIR"/textures/rainbow.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);
    if(_texid==0)
      std::cerr << "SOIL loading error: " << SOIL_last_result() << std::endl;

    AlignedBox3f aabb;
    for(auto obj:_meshes)
      aabb.extend(obj.mesh->boundingBox());

    reshape(w,h);
    _cam.setPerspective(M_PI/3,_nearPlane,20000.0f);
    _cam.lookAt(Vector3f(0,0.5,1)*aabb.sizes().maxCoeff()*1.3, aabb.center(), Vector3f(0,1,0));
    _trackball.setCamera(&_cam);

    // Texture rendering setup
    initTextureRendering();

    // NanoGUI setup
    _screen = new Screen();
    _screen->initialize(window, true);
    _gui = new FormHelper(_screen);
}

void Viewer::initTextureRendering()
{
    checkError();

    // Fullscreen quad VAO
    glGenVertexArrays(1, &_quadVAO);
    glBindVertexArray(_quadVAO);

    GLuint quadVBO;
    glGenBuffers(1, &quadVBO);
    glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(_screenQuadVertices), &_screenQuadVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)(3 * sizeof(float)));

//    std::string shader = "/shaders/screen_shader";
    std::string shader =  "/shaders/lic";
    loadScreenProgram(_currentScreenShader);
    _screen_shader.activate();
//    glUniform1i(_screen_shader.getUniformLocation("screenTexture"),0);
//    glUniform1i(_screen_shader.getUniformLocation("image"), 1);

    // Custom framebuffer
    glGenFramebuffers(1, &_fbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _fbo);

    // Screen texture
    glGenTextures(1, &_screenTexture);
    glBindTexture(GL_TEXTURE_2D, _screenTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _winWidth, _winHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL); // allocate texture without defining texels
    // Attach screen texture to FBO
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _screenTexture, 0);

    // Noise texture
    _noiseTexture = SOIL_load_OGL_texture(DATA_DIR"/textures/white_noise.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);
    glBindTexture(GL_TEXTURE_2D, _noiseTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

    // Set the list of draw buffers
    GLenum drawBuffers[] = {GL_COLOR_ATTACHMENT0};
    glDrawBuffers(1, drawBuffers);

    // Render buffer
    GLuint rbo;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, _winWidth, _winHeight);
    // Attach renderbuffer to FBO
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo);

    // Check framebuffer completeness
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
      std::cout << "Error : framebuffer not completed !" << std::endl;
      exit(EXIT_FAILURE);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    checkError();
}

void Viewer::reshape(int w, int h){
    glViewport(0, 0, w, h);
    _winWidth = w;
    _winHeight = h;
    _cam.setViewport(w,h);
}


/*!
   callback to draw graphic primitives
 */
void Viewer::display()
{ 
  glViewport(0, 0, _winWidth, _winHeight);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);

  Shader& prg = _main_shader;

  prg.activate();

  glUniform1i(prg.getUniformLocation("wireframe"), 0);
  glUniformMatrix4fv(prg.getUniformLocation("view_mat"),1,GL_FALSE,_cam.viewMatrix().data());
  glUniformMatrix4fv(prg.getUniformLocation("proj_mat"),1,GL_FALSE,_cam.projectionMatrix().data());

  glUniform1i(prg.getUniformLocation("disp_selection"),_disp_selection?1:0);
  glUniform1i(prg.getUniformLocation("wireframe"), 0);

  glBindTexture(GL_TEXTURE_2D, _texid);
  glActiveTexture(GL_TEXTURE0);
  glUniform1i(prg.getUniformLocation("colormap"), 0);

  if(_texturing)
    glUniform1f(prg.getUniformLocation("tex_blend_factor"), 1.0);
  else
    glUniform1f(prg.getUniformLocation("tex_blend_factor"), 0.0);

  if(_coldwarm_shading)
  {
    glUniform3f(prg.getUniformLocation("CoolColor"), 0.15,0.23,0.3);
    glUniform3f(prg.getUniformLocation("WarmColor"), 0.6,0.6,0.5);
  }
  else
  {
    glUniform3f(prg.getUniformLocation("CoolColor"), 0.3,0.3,0.3);
    glUniform3f(prg.getUniformLocation("WarmColor"), 0.7,0.7,0.7);
  }

  for(auto obj:_meshes)
  {
    if(obj.display)
    {
      setObjectMatrix(obj.transformation,prg);
      obj.mesh->draw(prg);
    }
  }

  if(_wireframe)
  {
    glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
    glEnable(GL_LINE_SMOOTH);
    glDepthFunc(GL_LEQUAL);
    glUniform1i(prg.getUniformLocation("wireframe"), 1);

    glUniform3fv(prg.getUniformLocation("color"),1,Vector3f(0.4,0.4,0.8).data());

    for(auto obj:_meshes)
    {
      if(obj.display)
      {
        setObjectMatrix(obj.transformation,prg);
        obj.mesh->draw(prg);
      }
    }

    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
    glUniform1i(prg.getUniformLocation("wireframe"), 0);
  }

  prg.deactivate();
}

void Viewer::setObjectMatrix(const Matrix4f &M, Shader& prg) const
{
  glUniformMatrix4fv(prg.getUniformLocation("obj_mat"),1,GL_FALSE,M.data());
  Matrix4f matLocal2Cam = _cam.viewMatrix() * M;
  Matrix3f matN = matLocal2Cam.topLeftCorner<3,3>().inverse().transpose();
  glUniformMatrix3fv(prg.getUniformLocation("normal_mat"),1,GL_FALSE,matN.data());
}

void Viewer::updateScene()
{
    checkError();
    if (_renderOnTexture)
    {
      // Redirect the drawing process to the custom framebuffer
      glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
      glEnable(GL_DEPTH_TEST);
    }

    // Draw scene the normal way
    display();

    if (_renderOnTexture)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glDisable(GL_DEPTH_TEST);
        glClearColor(0.24f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _screen_shader.activate();
        glUniform1i(_screen_shader.getUniformLocation("screenTexture"),0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, _screenTexture);
        if (_currentShader == ObjectShaderProgram::PRINC_CURV)
        {
          glUniform1i(_screen_shader.getUniformLocation("image"), 1);
          glActiveTexture(GL_TEXTURE1);
          glBindTexture(GL_TEXTURE_2D, _noiseTexture);
        }
        glBindVertexArray(_quadVAO);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        _screen_shader.deactivate();
    }
    checkError();

    // Draw GUI
    _screen->drawContents();
    _screen->drawWidgets();
}

void Viewer::loadProgram(int shader)
{
  _main_shader.loadFromFiles(DATA_DIR+_objectShaderPaths[shader]+".vert", DATA_DIR+_objectShaderPaths[shader]+".frag");
  checkError();

//  if (shader == ObjectShaderProgram::PRINC_CURV)
//    _renderOnTexture = true;
}

void Viewer::setCurrentProgram(int shader)
{
  _currentShader = shader;
  loadProgram(_currentShader);
}

void Viewer::loadScreenProgram(int shader)
{
  _screen_shader.loadFromFiles(DATA_DIR+_screenShaderPaths[shader]+".vert", DATA_DIR+_screenShaderPaths[shader]+".frag");
  checkError();
}

void Viewer::setCurrentScreenProgram(int shader)
{
  _currentScreenShader = shader;
  loadScreenProgram(_currentScreenShader);
}

////////////////////////////////////////////////////////////////////////////////
// Events


/*!
   callback to manage mouse : called when user press or release mouse button
   You can change in this function the way the user
   interact with the system.
 */
void Viewer::mousePressed(GLFWwindow *window, int button, int action, int mods)
{
  // trackball
  if(action == GLFW_PRESS)
  {
      if (button == GLFW_MOUSE_BUTTON_1)
      {
          _trackingMode = TM_ROTATE_AROUND;
          _trackball.start();
          _trackball.track(_lastMousePos);
      }
      else if (button == GLFW_MOUSE_BUTTON_3)
      {
          _trackingMode = TM_TRANSLATE;
      }
  }
  else if(action == GLFW_RELEASE)
  {
      _trackingMode = TM_NO_TRACK;
  }
}


/*!
   callback to manage mouse : called when user move mouse with button pressed
   You can change in this function the way the user
   interact with the system.
 */
void Viewer::mouseMoved(int x, int y)
{
  if(_trackingMode == TM_ROTATE_AROUND)
  {
      _trackball.track(Vector2i(x,y));
  }
  else if(_trackingMode == TM_TRANSLATE)
  {
      float tx = 2.0*float(x + 0.5f)/float(_winWidth)  - 1.;
      float ty =  -(2.0*float(y + 0.5f)/float(_winHeight) - 1.);
      _cam.translate(tx, ty);
  }

  _lastMousePos = Vector2i(x,y);

  _screen->cursorPosCallbackEvent(x, y);
}

void Viewer::mouseScroll(double x, double y)
{
  _cam.zoom(-0.1*y);
}

/*!
   callback to manage keyboard interactions
   You can change in this function the way the user
   interact with the system.
 */
void Viewer::keyEvent(int key, int scancode, int action, int mods)
{
  _screen->keyCallbackEvent(key, scancode, action ,mods);
}

void Viewer::charPressed(int key)
{
  if(key==GLFW_KEY_S || key=='s')
  {
    _disp_selection = !_disp_selection;
    std::cout << "Display selection: " << (_disp_selection?"enabled":"disabled") << std::endl;
  }
  else if(key==GLFW_KEY_W || key=='w')
  {
    _wireframe = !_wireframe;
    std::cout << "Wireframe: " << (_wireframe?"enabled":"disabled") << std::endl;
  }
  else if(key==GLFW_KEY_G || key=='g')
  {
    _coldwarm_shading = !_coldwarm_shading;
    std::cout << "Cold-warm shading: " << (_coldwarm_shading?"enabled":"disabled") << std::endl;
  }
  else if(key==GLFW_KEY_T || key=='t')
  {
    _texturing = !_texturing;
    std::cout << "Texturing: " << (_texturing?"enabled":"disabled") << std::endl;
  }
  else if(key==GLFW_KEY_M || key=='m')
  {
    _renderOnTexture = !_renderOnTexture;
  }
  else if(key == GLFW_KEY_R || key=='r')
  {
    loadProgram(_currentShader);
    loadScreenProgram(_currentScreenShader);
  }
}

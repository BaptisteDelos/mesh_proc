#include <read_ply.h>

#include <Eigen/Core>


bool readPLY(surface_mesh::Surface_mesh& mesh, const std::string& filename)
{
    FILE* file = fopen(filename.c_str(), "r");

    if (!file) {
        std::cerr << "readPLY: can only read from a file";
        return false;
    }

    VcgMesh m;
    vcg::tri::io::PlyInfo pi;

    if (vcg::tri::io::ImporterPLY<VcgMesh>::Open(m, filename.c_str(), pi) != 0) {
        std::cerr << "readPLY: error reading file " << filename << std::endl;
        return false;
    }

    std::cout << "Mesh has " << m.VN() << " vert and " << m.FN() << " faces\n";

    // From VcgMesh to Surface_mesh

    // Add vertices
    for (VcgMesh::VertexIterator vi = m.vert.begin(); vi != m.vert.end(); ++vi)
        mesh.add_vertex(surface_mesh::Point(vi->P().X(), vi->P().Y(), vi->P().Z()));

    // Add vertex colors
    surface_mesh::Surface_mesh::Vertex_property<Eigen::Vector3f> vcolors = mesh.add_vertex_property<Eigen::Vector3f>("v:color");
    surface_mesh::Surface_mesh::Vertex_iterator vit, vend = mesh.vertices_end();
    VcgMesh::VertexIterator vcg_vit, vcg_vend = m.vert.end();

    for (vit = mesh.vertices_begin(), vcg_vit = m.vert.begin(); vit != vend && vcg_vit != vcg_vend; ++vit, ++vcg_vit)
        vcolors[*vit] = Eigen::Vector3f(vcg_vit->C()[0], vcg_vit->C()[1], vcg_vit->C()[2])/255.;

    // Add vertex texture coordinates
    surface_mesh::Surface_mesh::Vertex_property<Eigen::Vector2f> vtexcoord = mesh.add_vertex_property<Eigen::Vector2f>("v:texcoord");

    for (vit = mesh.vertices_begin(), vcg_vit = m.vert.begin(); vit != vend && vcg_vit != vcg_vend; ++vit, ++vcg_vit)
        vtexcoord[*vit] = Eigen::Vector2f(vcg_vit->T().P().X(), vcg_vit->T().P().Y());

    // Add faces
    for (VcgMesh::FaceIterator fi = m.face.begin(); fi != m.face.end(); ++fi) {
        std::vector<surface_mesh::Surface_mesh::Vertex> f;
        surface_mesh::Surface_mesh::Vertex v0( (unsigned int)(fi->V(0) - &(*m.vert.begin())) );
        surface_mesh::Surface_mesh::Vertex v1( (unsigned int)(fi->V(1) - &(*m.vert.begin())) );
        surface_mesh::Surface_mesh::Vertex v2( (unsigned int)(fi->V(2) - &(*m.vert.begin())) );

        f.push_back(v0);
        f.push_back(v1);
        f.push_back(v2);

        mesh.add_face(f);
    }

    return true;
}

#include "opengl.h"
#include "mesh_processing_app.h"
#include <SOIL2.h>
#include <iostream>

#include <nanogui/nanogui.h>

Viewer* v;

int WIDTH = 1000;
int HEIGHT = 1000;

int g_pixel_ratio = 1;
bool wireframe = false;

// NanoGui variables

using namespace nanogui;

Screen *screen = nullptr;

static void char_callback(GLFWwindow* /*window*/, unsigned int key)
{
  v->charPressed(key);
}

static void scroll_callback(GLFWwindow* /*window*/, double x, double y)
{
  v->mouseScroll(x,y);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
  if ((key == GLFW_KEY_ESCAPE) && action == GLFW_PRESS) {
      glfwSetWindowShouldClose(window, true);
  } else if ((key == GLFW_KEY_L) && action == GLFW_PRESS) {
      wireframe = !wireframe;
      GLenum mode = wireframe ? GL_LINE : GL_FILL;
      glPolygonMode(GL_FRONT_AND_BACK, mode);
  }

  v->keyEvent(key,scancode,action,mods);
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
  v->mousePressed(window, button, action, mods);
}

void cursorPos_callback(GLFWwindow* /*window*/, double x, double y)
{
  v->mouseMoved(x*g_pixel_ratio,y*g_pixel_ratio);
}

void reshape_callback(GLFWwindow* window, int width, int height)
{
  v->reshape(width,height);
  v->updateScene();
  glfwSwapBuffers(window);
}

// initialize GLFW framework
GLFWwindow* initGLFW()
{
  if (!glfwInit())
      exit(EXIT_FAILURE);

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, (GLint)GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Surface Modeling", NULL, NULL);

  // Subsidiary part : load icon for the app (https://stackoverflow.com/questions/44321902/glfw-setwindowicon)
  GLFWimage icons[1];
  icons[0].pixels = SOIL_load_image("../data/images/dame_a_la_capuche.png", &icons[0].width, &icons[0].height, 0, SOIL_LOAD_RGBA);
  glfwSetWindowIcon(window, 1, icons);
  SOIL_free_image_data(icons[0].pixels);

  if (!window) {
      glfwTerminate();
      exit(EXIT_FAILURE);
  }
  glfwMakeContextCurrent(window);
  glbinding::Binding::initialize();
  std::string glfw_version{glfwGetVersionString()};
  std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;
  std::cout << "GLSL version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
  std::cout << "GLFW version: " << glfw_version << std::endl;

  // Handle retina display:
  int w, h;
  glfwGetFramebufferSize(window, &w, &h);
  if(w==2*WIDTH)
    g_pixel_ratio = 2;

  return window;
}

static void install_callbacks(GLFWwindow* window)
{
    glfwSetKeyCallback(window, key_callback);
    glfwSetCharCallback(window, char_callback);
    glfwSetFramebufferSizeCallback(window, reshape_callback);
    glfwSetCursorPosCallback(window, cursorPos_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetScrollCallback(window, scroll_callback);
}

static void error_callback(int /*error*/, const char* description)
{
  fputs(description, stderr);
}


int main (int argc, char **argv)
{
  glfwSetErrorCallback(error_callback);

  GLFWwindow* window = initGLFW();
  int w, h;
  glfwGetFramebufferSize(window, &w, &h);

  v = new MeshProcessingApp();

  v->init(window,w,h,argc,argv);

  install_callbacks(window);

  double t0 = glfwGetTime();
  double t1 = t0;
  while (!glfwWindowShouldClose(window))
  {
    // render the scene
    t1 = glfwGetTime();
    if((t1-t0)>0.04) {
      v->updateScene();
      glfwSwapBuffers(window);
      t0 = t1;
    }
    glfwPollEvents();
  }

  delete v;

  glfwDestroyWindow(window);
  glfwTerminate();

  exit(EXIT_SUCCESS);
}


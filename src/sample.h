#ifndef SAMPLE_H
#define SAMPLE_H

#include <patate/Patate/grenaille.h>


class Sample
{
public:
    enum {Dim = 3};
    typedef float Scalar;
    typedef Eigen::Matrix<Scalar, Dim, 1> VectorType;
    typedef Eigen::Matrix<Scalar, Dim+1, 1> HVectorType;
    typedef Eigen::Matrix<Scalar, Dim, Dim> MatrixType;

    MULTIARCH inline Sample(const VectorType& pos = VectorType::Zero(), const VectorType& normal = VectorType::Zero())
        : _pos(pos), _normal(normal) {}

    MULTIARCH inline const VectorType& pos()    const { return _pos; }
    MULTIARCH inline const VectorType& normal() const { return _normal; }

    MULTIARCH inline       VectorType& pos()          { return _pos; }
    MULTIARCH inline       VectorType& normal()       { return _normal; }

private:
    VectorType _pos, _normal;
};

#endif // SAMPLE_H


//=============================================================================
//                                   MESH_H
//=============================================================================

#ifndef MESH_H
#define MESH_H


//== INCLUDES =================================================================

#include "ray.h"

#include <iostream>
#include <surface_mesh/surface_mesh.h>

#include <string>
#include <vector>
#include <Eigen/Core>
#include <sample.h>
#include "opengl.h"


//== CLASS DEFINITION =========================================================

class BVH;
class Shader;

class Mesh : public surface_mesh::Surface_mesh
{
    using Vector3f = Eigen::Vector3f;
    using Vector2f = Eigen::Vector2f;

public:

    enum Mask {
        NONE             = 0x00000,

        VERT_COORD       = 0x00001,
        VERT_COLOR       = 0x00002,
        VERT_NORMAL      = 0x00004,
        VERT_TEXCOORD    = 0x00008,

        // Mean curvature
        VERT_KAPPA       = 0x00010,

        // GLS principal curvatures
        VERT_K1          = 0x00020,
        VERT_K2          = 0x00040,
        VERT_K1_DIR      = 0x00080,
        VERT_K2_DIR      = 0x00100,
        VERT_TRUST       = 0x00200,
        VERT_MASK        = 0x00400,

        FACE_INDEX       = 0x01000,
        FACE_COLOR       = 0x02000,
        FACE_NORMAL      = 0x04000,

        ALL              = 0xFFFFF
    };

    Mesh() : _ready(false), _bvh(0) {}
    ~Mesh();

    /// Loads the mesh from file
    void load(const std::string& filename);

    /// Creates a mesh representing a uniform paramteric grids with m x n vertices over the [0,1]^2 range.
    void createGrid(int m, int n);

    void init();

    /// Draws the mesh using OpenGL
    void draw(const Shader &shader);

    /// returns face indices as a 3xN matrix of integers
    Eigen::Map<Eigen::Matrix<int,3,Eigen::Dynamic> > faceIndices()
    { return Eigen::Map<Eigen::Matrix<int,3,Eigen::Dynamic> >(_indices.data(), 3, n_faces()); }

    Eigen::Map<const Eigen::Matrix<int,3,Eigen::Dynamic> > faceIndices() const
    { return Eigen::Map<const Eigen::Matrix<int,3,Eigen::Dynamic> >(_indices.data(), 3, n_faces()); }

    // Accessors to vertex attributes as Eigen's matrices:
    Eigen::Map<Eigen::Matrix3Xf> positions()
    {
      auto& vertices = get_vertex_property<Vector3f>("v:point").vector();
      return Eigen::Map<Eigen::Matrix3Xf>(vertices[0].data(), 3, vertices.size());
    }

    Eigen::Map<Eigen::Matrix3Xf> colors()
    {
      auto& vcolors = get_vertex_property<Vector3f>("v:color").vector();
      return Eigen::Map<Eigen::Matrix3Xf>(vcolors[0].data(), 3, vcolors.size());
    }

    Eigen::Map<Eigen::Matrix3Xf> normals()
    {
      auto& vnormals = get_vertex_property<Vector3f>("v:normal").vector();
      return Eigen::Map<Eigen::Matrix3Xf>(vnormals[0].data(), 3, vnormals.size());
    }

    Eigen::Map<Eigen::Matrix2Xf> texcoords()
    {
      auto& texcoords = get_vertex_property<Vector2f>("v:texcoord").vector();
      return Eigen::Map<Eigen::Matrix2Xf>(texcoords[0].data(), 2, texcoords.size());
    }

    Eigen::Map<Eigen::VectorXi> masks()
    {
      auto& masks = get_vertex_property<int>("v:mask").vector();
      return Eigen::Map<Eigen::VectorXi>(masks.data(), masks.size());
    }

    /// Call all update* functions below.
    void updateAll();

    /// Re-compute vertex normals (needs to be called after editing vertex positions)
    void updateNormals();

    /// Re-compute the aligned bounding box (needs to be called after editing vertex positions)
    void updateBoundingBox();

    /// Re-compute the BVH for fast ray-mesh intersections (needs to be called after editing vertex positions)
    void updateBVH();

    /// Copy vertex attributes from the CPU to GPU memory if those have to be updated (needs to be called after editing any vertex attributes: positions, normals, texcoords, masks, etc.)
    void updateVBO(int vbo_to_update);

    /// computes the first intersection between the ray and the mesh in hit (if any)
    bool intersect(const Ray& ray, Hit& hit) const;

    /// Verify the existence of the property of type T and with name specified in parameter
    template<typename T>
    bool hasProperty(const std::string name) const;

    /// Proceed with the Grenaille OrientedSphereFit procedure and register the resulting GLS parameters to describe curvature
    void fit(float t);


    void updateSelection(void);


    void computeScaleWeights(float t);


    void passTwo(float t);


    std::vector< std::pair<float, int> >& histogram_h1k1(void) { return _histh1k1; };
    std::vector< std::pair<float, int> >& histogram_h1k2(void) { return _histh1k2; };


    // For BVH:

    /** compute the intersection between a ray and a given triangular face */
    bool intersectFace(const Ray& ray, Hit& hit, int faceId) const;

    const Eigen::AlignedBox3f& boundingBox() const { return _bbox; }


    float& alpha();
    float& alpha_bar();
    float& beta();
    float& beta_bar();

    void setAlpha(float alpha);
    void setAlpha_bar(float alpha_bar);
    void setBeta(float beta);
    void setBeta_bar(float beta_bar);

private:

    bool _ready;
    Eigen::AlignedBox3f _bbox;

    std::vector<int> _indices;
    gl::GLuint  _faces_vbo_id;

    BVH *_bvh;

    GLuint _vao;

    struct AttributeProp {
      std::string                  name;
      std::string               gl_name;
      std::size_t nb_coeffs_per_element;
      std::size_t  nb_bytes_per_element;
      gl::GLenum         gl_scalar_type;
      std::size_t                  size;
      const void*                  data;
      gl::GLuint                 vbo_id;
      int                          mask;
    };

    std::vector<AttributeProp> _attributeProperties;


    std::vector<Vertex> _selectedVertices;


    std::vector< std::pair<float, int> > _histh1k1;
    std::vector< std::pair<float, int> > _histh1k2;

    float     _alpha = 6.;
    float _alpha_bar = 2.;
    float      _beta = 8000.;
    float  _beta_bar = 10.;


    template<typename T>
    void register_property(const char* name, const char* gl_name, int vbo_mask);

    std::string replace(const char* str, const char* from, const char* to);

    Sample vertexToSample(Vertex v);

    std::vector< std::pair<float, int> > histogram(float min, float max, int ninter, std::vector<float> values);
};


#endif // MESH_H

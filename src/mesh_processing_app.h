
#ifndef MESH_PROCESSING_APP_H
#define MESH_PROCESSING_APP_H

#include "viewer.h"
#include <nanogui/graph.h>

class MeshProcessingApp : public Viewer {
public:
  //! Constructor
  MeshProcessingApp();
  ~MeshProcessingApp();

  void init(GLFWwindow* window, int w, int h, int argc, char **argv);
  void updateScene();

  // events
  void mousePressed(GLFWwindow* window, int button, int action, int mods);
  void mouseMoved(int x, int y);
  void mouseScroll(double x, double y);
  void keyEvent(int key, int scancode, int action, int mods);
  void charPressed(int key);

protected:

  bool pickAt(const Eigen::Vector2f &p, Hit &hit) const;
  bool selectAround(const Eigen::Vector2f &p) const;
  void selectAll(void) const;
  void invertSelection(void) const;
  bool mouseOnWidget(void) const;
  void initGUI(void);

  template<typename T>
  void displayParameter(const std::string& name, T value);

  // main mesh to work on:
  Mesh* _mesh;

  float _pickingRadius = 0.1f;
  bool _pickingMode = false;

  // Fitting process
  float _neighborhoodSize;

  // GUI components
  Window* _GLSWindow;
  Window* _selectionWindow;
  Window* _histogramWindow;

  detail::FormWidget<float> *_textFieldNeiSize;
  detail::FormWidget<float> *_textFieldPickRadius;
  detail::FormWidget<float> *_textFieldAlpha;
  detail::FormWidget<float> *_textFieldAlphaBar;
  detail::FormWidget<float> *_textFieldBeta;
  detail::FormWidget<float> *_textFieldBetaBar;
  Graph* _graph_h1k1;
  Graph* _graph_h1k2;
  Button* _multiPassButton;

};

#endif // MESH_PROCESSING_APP_H

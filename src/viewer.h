#ifndef VIEWER_H
#define VIEWER_H

#include "opengl.h"
#include "shader.h"
#include "camera.h"
#include "trackball.h"
#include "mesh.h"

#include <nanogui/nanogui.h>
#include <iostream>

using namespace nanogui;

struct Object3D {
  Object3D(Mesh* m, Shader* s) : mesh(m), prg(s) {}
  Mesh* mesh = 0;
  Eigen::Matrix4f transformation = Eigen::Matrix4f::Identity();
  Shader* prg = 0;
  bool display = true;
};

class Viewer {
public:
  enum ObjectShaderProgram
  {
    BLINN=0, MEAN_CURV, GAUSSIAN_CURV, PRINC_CURV, TRUST, ENGRAVING
  };
  enum ScreenShaderProgram
  {
    NEUTRAL=0, LIC
  };

  //! Constructor
  Viewer();
  virtual ~Viewer();

  // OpenGL stuff

  /// Automatically called to redraw the scene
  void display();

  /// Automatically called when the window is resized
  void reshape(int w, int h);

  /// Call this function to reload the shader from files
  void loadProgram(int shader);


  void setCurrentProgram(int shader);


  void loadScreenProgram(int shader);


  void setCurrentScreenProgram(int shader);

  /// Called once to initialize the scene
  virtual void init(GLFWwindow* window, int w, int h, int argc, char **argv);

  /// Called
  virtual void updateScene();

  // events
  virtual void mousePressed(GLFWwindow* window, int button, int action, int mods);
  virtual void mouseMoved(int x, int y);
  virtual void mouseScroll(double x, double y);
  virtual void keyEvent(int key, int scancode, int action, int mods);
  virtual void charPressed(int key);

protected:
  void setObjectMatrix(const Eigen::Matrix4f &M, Shader& prg) const;
  void initTextureRendering(void);


protected:
  int _winWidth, _winHeight;

  Camera _cam;
  float _nearPlane = 0.01f;

  // NanoGUI parameters

  Screen*  _screen;
  FormHelper* _gui;

  // the default shader program
  Shader _main_shader;
  std::vector<std::string> _objectShaderPaths =
  {
    "/shaders/blinn",
    "/shaders/mean_curvature",
    "/shaders/gaussian_curvature",
    "/shaders/principal_curvature",
    "/shaders/trust",
    "/shaders/engraving"
  };
  std::vector<std::string> _screenShaderPaths =
  {
    "/shaders/screen_shader",
    "/shaders/lic"
  };

  int _currentShader;
  int _currentScreenShader;
  int _texid;

  // list of meshes to display
  std::vector<Object3D> _meshes;

  // Multi-pass rendering
  GLuint _fbo;
  Shader _screen_shader;
  bool _renderOnTexture = false;
  GLuint _screenTexture;
  GLuint _noiseTexture;
  GLuint _quadVAO;
  GLfloat _screenQuadVertices[30] = {
      // Positions        // UV
      -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
       1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
      -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
      -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
       1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
       1.0f,  1.0f, 0.0f, 1.0f, 1.0f
  };

  // Mouse parameters for the trackball
  enum TrackMode
  {
    TM_NO_TRACK=0, TM_ROTATE_AROUND, TM_ZOOM,
    TM_LOCAL_ROTATE, TM_FLY_Z, TM_FLY_PAN,
    TM_TRANSLATE
  };
  TrackMode _trackingMode = TM_NO_TRACK;
  Trackball _trackball;
  Eigen::Vector2i _lastMousePos;

  bool _disp_selection = true;
  bool _wireframe = false;
  bool _texturing = false;
  bool _coldwarm_shading = true;
};

#endif

﻿
#include "mesh_processing_app.h"
#include "neighborhood.h"
#include "laplacian.h"
#include <nanogui/layout.h>

using namespace Eigen;
using namespace surface_mesh;

MeshProcessingApp::MeshProcessingApp()
 : Viewer(), _mesh(0)
 {
 }

MeshProcessingApp::~MeshProcessingApp()
{
  delete _mesh;
  delete _GLSWindow;
  delete _selectionWindow;
}

void MeshProcessingApp::init(GLFWwindow* window, int w, int h, int argc, char **argv)
{
  std::cout << "Usage: " << argv[0] << " [file]" << std::endl;
  std::cout << "  where must be a mesh file (e.g. .off) " << std::endl << std::endl;
  std::cout << "**************************************************" << std::endl;
  std::cout << "Selection:" << std::endl;
  std::cout << "  ctrl+left+drag: selection" << std::endl;
  std::cout << "  s:    enable/disable display of selected vertices" << std::endl;
  std::cout << "  c:    clear the selection" << std::endl;
  std::cout << "Mesh processing:" << std::endl;
  std::cout << "  y/Y:  decrease/increase selection brush size" << std::endl << std::endl;
  std::cout << "  1:    perform harmonic interpolation" << std::endl;
  std::cout << "  2:    perform bi-harmonic interpolation" << std::endl;
  std::cout << "  3:    perform tri-harmonic interpolation" << std::endl;



  _mesh = new Mesh();
  if(argc>1)
    _mesh->load(argv[1]);
  else
  {
//   _mesh->load(DATA_DIR"/models/sphere.off");
    _mesh->load(DATA_DIR"/models/bunny70k.off");
//   _mesh->load(DATA_DIR"/models/colorfull.off");
  }
  _mesh->init();
  _meshes.push_back(Object3D(_mesh,&_main_shader));

  _pickingRadius = 0.1f * _mesh->boundingBox().sizes().maxCoeff();
  _neighborhoodSize = 0.01f * _mesh->boundingBox().sizes().maxCoeff();

  // Display initial parameter values
  displayParameter<float>("t", _neighborhoodSize);

  Viewer::init(window,w,h,argc,argv);

  initGUI();
}

void MeshProcessingApp::initGUI()
{
    // Add windows
    _selectionWindow = _gui->addWindow(Eigen::Vector2i(10,10), "Selection");
    _selectionWindow->setSize(Eigen::Vector2i(180, 120));
    _gui->addButton("Select all", [this] (void) {
                      selectAll();
                      _mesh->updateSelection();
                      _mesh->updateVBO(Mesh::Mask::VERT_COLOR | Mesh::Mask::VERT_MASK);
          })->setTooltip("A");
    _gui->addButton("Invert selection", [this] (void) {
                       invertSelection();
                       _mesh->updateSelection();
                       _mesh->updateVBO(Mesh::Mask::VERT_COLOR | Mesh::Mask::VERT_MASK);
          })->setTooltip("I");
    _textFieldPickRadius = _gui->addVariable("Picking radius", _pickingRadius);
    _textFieldPickRadius->setCallback([this] (const float t) {
            _pickingRadius = t;
          });
    _textFieldPickRadius->setEditable(true);

    _GLSWindow = _gui->addWindow(Eigen::Vector2i(_selectionWindow->width() + 20, 10), "GLS parameters");
    _GLSWindow->setSize(Eigen::Vector2i(350, 120));
    _textFieldNeiSize = _gui->addVariable("Neighborhood size (t)", _neighborhoodSize);
    _textFieldNeiSize->setCallback([this] (const float t) {
            _neighborhoodSize = t;
          });
    _textFieldNeiSize->setEditable(true);
    Button* computeWeightsButton = _gui->addButton("Compute scale weights", [this] (void) {
                      int mask = Mesh::Mask::VERT_K1 | Mesh::Mask::VERT_K2;
                      _mesh->computeScaleWeights(_neighborhoodSize);
                      _mesh->updateVBO(mask);
                      VectorXf &func_h1k1 = _graph_h1k1->values();
                      VectorXf &func_h1k2 = _graph_h1k2->values();

                      auto hist_h1k1 = _mesh->histogram_h1k1();
                      auto hist_h1k2 = _mesh->histogram_h1k2();

                      func_h1k1.resize(hist_h1k1.size());
                      func_h1k2.resize(hist_h1k2.size());
                      for (int i = 0; i < hist_h1k1.size(); ++i)
                      {
                        func_h1k1[i] = hist_h1k1[i].second;
                        func_h1k2[i] = hist_h1k2[i].second;
                      }

                      _textFieldAlpha->setValue(_mesh->alpha());
                      _textFieldAlphaBar->setValue(_mesh->alpha_bar());
                      _textFieldBeta->setValue(_mesh->beta());
                      _textFieldBetaBar->setValue(_mesh->beta_bar());
          });
    computeWeightsButton->setTooltip("Compute alpha and beta factors from which trust weights are deduced.");
    Button* fitButton = _gui->addButton("Oriented Sphere Fit", [this] (void) {
                      int mask = Mesh::Mask::VERT_KAPPA | Mesh::Mask::VERT_K1 | Mesh::Mask::VERT_K2 | Mesh::Mask::VERT_K1_DIR | Mesh::Mask::VERT_K2_DIR | Mesh::Mask::VERT_TRUST;
                      _mesh->fit(_neighborhoodSize);
                      _mesh->updateVBO(mask);
          });
    fitButton->setTooltip("Fit a point set surface with oriented algebraic spheres.");
    _multiPassButton = _gui->addButton("Render on texture", NULL);
    _multiPassButton->setFlags(Button::ToggleButton);
    _multiPassButton->setChangeCallback([this] (bool state) { _renderOnTexture = state; });
    _multiPassButton->setTooltip("Two passes rendering for post-processing work.");
    Button* passTwoButton = _gui->addButton("Fit a plane, project, fit a sphere in 2D", [this] (void) {
                      int mask = Mesh::Mask::VERT_COLOR | Mesh::Mask::VERT_KAPPA | Mesh::Mask::VERT_K1 | Mesh::Mask::VERT_K2 | Mesh::Mask::VERT_K1_DIR | Mesh::Mask::VERT_K2_DIR | Mesh::Mask::VERT_TRUST;
                      _mesh->passTwo(_neighborhoodSize);
                      _mesh->updateVBO(mask);
          });
    fitButton->setTooltip("Fit a point set surface with oriented spheres.");
    ObjectShaderProgram objectshader = ObjectShaderProgram(_currentShader);
    detail::FormWidget<ObjectShaderProgram> *objectShaderComboBox = _gui->addVariable("Object shader", objectshader, true);
    objectShaderComboBox->setItems({ "                  Blinn                  ",
                                     "            Mean curvature               ",
                                     "           Gaussian curvature            ",
                                     "          Principal curvature            ",
                                     "                  Trust                  ",
                                     "               Engravings                " });
    objectShaderComboBox->setCallback([this] (ObjectShaderProgram shader) {
                      Viewer::setCurrentProgram(shader);
    });
    ScreenShaderProgram screenshader = ScreenShaderProgram(_currentScreenShader);
    detail::FormWidget<ScreenShaderProgram> *screenShaderComboBox = _gui->addVariable("Screen shader", screenshader, true);
    screenShaderComboBox->setItems({ "                 Neutral                 ",
                                     "        Line Integral Convolution        " });
    screenShaderComboBox->setCallback([this] (ScreenShaderProgram shader) {
                      Viewer::setCurrentScreenProgram(shader);
    });

    _histogramWindow = _gui->addWindow(Eigen::Vector2i(_selectionWindow->width() + _GLSWindow->width() + 40, 10), "Scale parameters");
    _histogramWindow->setSize(Eigen::Vector2i(350, 120));

    _textFieldBeta = _gui->addVariable("Beta", _mesh->beta(), false);
    _textFieldBeta->setCallback([this] (float beta) { _mesh->setBeta(beta); });
    _textFieldBeta->setEditable(true);
    _textFieldBetaBar = _gui->addVariable("Beta bar", _mesh->beta_bar(), false);
    _textFieldBetaBar->setCallback([this] (float beta_bar) { _mesh->setBeta_bar(beta_bar); });
    _textFieldBetaBar->setEditable(true);
    _textFieldAlpha = _gui->addVariable("Alpha", _mesh->alpha(), false);
    _textFieldAlpha->setCallback([this] (float alpha) { _mesh->setAlpha(alpha); });
    _textFieldAlpha->setEditable(true);
    _textFieldAlphaBar = _gui->addVariable("Alpha bar", _mesh->alpha_bar(), false);
    _textFieldAlphaBar->setCallback([this] (float alpha_bar) { _mesh->setAlpha_bar(alpha_bar); });
    _textFieldAlphaBar->setEditable(true);

    _histogramWindow->setLayout(new GroupLayout());
    _graph_h1k1 = _histogramWindow->add<Graph>("");
    _graph_h1k2 = _histogramWindow->add<Graph>("");
    _graph_h1k2->setHeader("|k2|*h1");

    _screen->setVisible(true);
    _screen->performLayout();
}


bool MeshProcessingApp::pickAt(const Eigen::Vector2f &p, Hit &hit) const
{
  Matrix4f proj4 = _cam.projectionMatrix();
  Matrix3f proj3;
  proj3 << proj4.topLeftCorner<2,3>(),
           proj4.bottomLeftCorner<1,3>();
  Matrix4f C = _cam.viewMatrix().inverse();

  Vector3f q(  (2.0*float(p.x() + 0.5f)/float(_winWidth)  - 1.),
              -(2.0*float(p.y() + 0.5f)/float(_winHeight) - 1.),
                1);

  Ray ray;
  ray.origin = C.col(3).head<3>();
  ray.direction = C.topLeftCorner<3,3>() * (proj3.inverse() * q);
  return _mesh->intersect(ray, hit);
}

bool MeshProcessingApp::selectAround(const Eigen::Vector2f &p) const
{
  Hit hit;
  if(pickAt(p,hit))
  {
    // Here we found the closest triangle under the mouse pointer (its index is "hit.faceId()").
    // Let's found the closest vertex and extend the selection through select_neighbors:
    int clostest_id = 0;
    hit.baryCoords().maxCoeff(&clostest_id);
    Surface_mesh::Vertex closest_v(_mesh->faceIndices()(clostest_id,hit.faceId()));
    std::vector<Surface_mesh::Vertex> neighbors = select_neighbors(*_mesh, closest_v, _pickingRadius);
    auto masks = _mesh->get_vertex_property<int>("v:mask");
    for(auto v:neighbors)
      masks[v] = 1;
    return true;
  }
  return false;
}

void MeshProcessingApp::selectAll() const
{
    auto masks = _mesh->masks();
    masks.fill(1);
}

void MeshProcessingApp::invertSelection() const
{
    auto masks = _mesh->get_vertex_property<int>("v:mask");
    for (auto vit = _mesh->vertices_begin(); vit != _mesh->vertices_end(); ++vit) {
        int m = masks[*vit];
        masks[*vit] = (m-1) * (m-1);
    }
}

bool MeshProcessingApp::mouseOnWidget() const
{
    int i = 0;
    Eigen::Vector2i mousePos = _screen->mousePos();

    for (Widget* w : _screen->children())
    {
        if (w->contains(mousePos))
            break;
        ++i;
    }

    return i != _screen->childCount();
}

/*!
   callback to manage mouse : called when user press or release mouse button
   You can change in this function the way the user
   interact with the system.
 */
void MeshProcessingApp::mousePressed(GLFWwindow *window, int button, int action, int mods)
{
  if (mouseOnWidget())
  {
    _screen->mouseButtonCallbackEvent(button, action, mods);
    return;
  }

  if((mods&GLFW_MOD_CONTROL)==GLFW_MOD_CONTROL)
  {
    if(action == GLFW_PRESS)
    {
      // picking
      _pickingMode = true;
      _disp_selection = true;
      if(selectAround(_lastMousePos.cast<float>()))
      {
        int mask = Mesh::Mask::VERT_COLOR | Mesh::Mask::VERT_MASK; // update vertex colors and mask
        _mesh->updateSelection();
        _mesh->updateVBO(mask);
      }
    }
    else
    {
      _pickingMode = false;
    }
  }
  else
  {
    if (action != GLFW_RELEASE)
    {
      for (Widget* w : _screen->children())
      {
        w->setFocused(false);
      }
    }

    Viewer::mousePressed(window,button,action,mods);
  }
}

/*!
   callback to manage mouse : called when user move mouse with button pressed
   You can change in this function the way the user
   interact with the system.
 */
void MeshProcessingApp::mouseMoved(int x, int y)
{
  if(_pickingMode)
  {
    if(selectAround(Vector2f(x,y))) {
      int mask = Mesh::Mask::VERT_COLOR | Mesh::Mask::VERT_MASK; // update vertex colors and mask
      _mesh->updateSelection();
      _mesh->updateVBO(mask);
    }
  }
  else
  {
    Viewer::mouseMoved(x,y);
  }
  _lastMousePos = Vector2i(x,y);
}

void MeshProcessingApp::mouseScroll(double x, double y)
{
  if (mouseOnWidget())
  {
    _screen->scrollCallbackEvent(x,y);
  }
  else
  {
    Viewer::mouseScroll(x, y);
  }
}

void MeshProcessingApp::keyEvent(int key, int scancode, int action, int mods)
{

}

void MeshProcessingApp::updateScene()
{
  Viewer::updateScene();
}

void MeshProcessingApp::charPressed(int key)
{
  if (mouseOnWidget())
  {
      _screen->charCallbackEvent(static_cast<unsigned int>(key));
      return;
  }

  int mask;

  switch(key)
  {
    case GLFW_KEY_Y:
    {
      _pickingRadius *= 1.2;
      _textFieldPickRadius->setValue(_pickingRadius);
      break;
    }
    case 'y':
    {
      _pickingRadius /= 1.2;
      _textFieldPickRadius->setValue(_pickingRadius);
      break;
    }
    case 'c':
    {
      mask = Mesh::Mask::VERT_COLOR | Mesh::Mask::VERT_MASK; // update vertex colors and mask
      _mesh->masks().setZero();
      _mesh->updateSelection();
      _mesh->updateVBO(mask);
      break;
    }
    case 'f':
    {
      int mask = Mesh::Mask::VERT_KAPPA | Mesh::Mask::VERT_K1 | Mesh::Mask::VERT_K2 | Mesh::Mask::VERT_K1_DIR | Mesh::Mask::VERT_K2_DIR | Mesh::Mask::VERT_TRUST;
      _mesh->fit(_neighborhoodSize);
      _mesh->updateVBO(mask); // update GLS params
      break;
    }
    case GLFW_KEY_1:
    case GLFW_KEY_2:
    case GLFW_KEY_3:
    case GLFW_KEY_4:
    {
      setCurrentProgram(key-GLFW_KEY_1);
      break;
    }
    case '+':
    case '-':
    {
      if (key=='+')
        _neighborhoodSize *= 1.5;
      else
        _neighborhoodSize /= 1.5;

      _textFieldNeiSize->setValue(_neighborhoodSize);

      if (_currentShader & (ObjectShaderProgram::MEAN_CURV || ObjectShaderProgram::GAUSSIAN_CURV || ObjectShaderProgram::PRINC_CURV))
      {
        _mesh->fit(_neighborhoodSize);
        int mask = Mesh::Mask::VERT_KAPPA | Mesh::Mask::VERT_K1 | Mesh::Mask::VERT_K2;
        _mesh->updateVBO(mask);
        displayParameter<float>("t", _neighborhoodSize);
      }
      break;
    }
    case 'a':
    {
      mask = Mesh::Mask::VERT_COLOR | Mesh::Mask::VERT_MASK; // update vertex colors and mask
      selectAll();
      _mesh->updateSelection();
      _mesh->updateVBO(mask);
      break;
    }
    case 'i':
    {
      mask = Mesh::Mask::VERT_COLOR | Mesh::Mask::VERT_MASK; // update vertex colors and mask
      invertSelection();
      _mesh->updateSelection();
      _mesh->updateVBO(mask);
      break;
    }
    case 'm':
    {
      Viewer::charPressed(key);
      _multiPassButton->setPushed(_renderOnTexture);
      break;
    }
    default:
    {
      Viewer::charPressed(key);
      break;
    }
  }
}

template<typename T>
void MeshProcessingApp::displayParameter(const std::string &name, T value)
{
    std::cout << "[mesh_processing_app] Parameter " << name << " has value " << value << std::endl;
}

#include "mesh.h"
#include "bvh.h"
#include "shader.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <stack>
#include <cmath>

#include <patate/Patate/grenaille.h>
#include <read_ply.h>
#include <neighborhood.h>

using namespace std;
using namespace Eigen;
using namespace surface_mesh;
using namespace Grenaille;

template<typename T>
gl::GLenum gl_type();

template<> gl::GLenum gl_type<int>() { return GL_INT; }
template<> gl::GLenum gl_type<float>() { return GL_FLOAT; }
template<> gl::GLenum gl_type<double>() { return GL_DOUBLE; }

template<typename T, int = T::SizeAtCompileTime>
size_t size_at_compile_time() { return T::SizeAtCompileTime; }

template<typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value>::type >
size_t size_at_compile_time() { return 1; }

template<typename T, bool = std::is_arithmetic<T>::value> struct value_type {
  typedef T type;
};

template<typename T> struct value_type<T,false> {
  typedef typename T::Scalar type;
};

Sample Mesh::vertexToSample(Vertex v) {
    auto vpositions = get_vertex_property<Vector3f>("v:point");
    auto vnormals = get_vertex_property<Vector3f>("v:normal");
    auto point = Eigen::Matrix<Sample::Scalar, 3, 1>({vpositions[v][0], vpositions[v][1], vpositions[v][2]});
    auto normal = Eigen::Matrix<Sample::Scalar, 3, 1>({vnormals[v][0], vnormals[v][1], vnormals[v][2]});
    Sample s{point, normal};

    return s;
}


template<typename T>
void Mesh::register_property(const char* name, const char* gl_name, int vbo_mask) {
    auto prop = get_vertex_property<T>(name);
    AttributeProp attrProp{
        name,
        gl_name,
        size_at_compile_time<T>(),
        sizeof(T),
        gl_type<typename value_type<T>::type>(),
        prop.vector().size(),
        prop.data(),
        gl::GLuint(0),
        vbo_mask
    };
    _attributeProperties.emplace_back(attrProp);
}


Mesh::~Mesh()
{
    GLuint vbo[_attributeProperties.size()];
    for (unsigned int i = 0; i < _attributeProperties.size(); ++i)
        vbo[i] = _attributeProperties[i].vbo_id;

    if(_ready){
        glDeleteBuffers(_attributeProperties.size(), vbo);
        glDeleteVertexArrays(1,&_vao);
    }
    delete _bvh;
}


void Mesh::load(const string& filename)
{
    cout << "Loading file: " << filename << endl;

    // extract file extension
    std::string::size_type dot(filename.rfind("."));
    if (dot == std::string::npos)
    {
        std::cout << dot << std::endl;
        return;
    }
    std::string ext = filename.substr(dot+1, filename.length()-dot-1);
    std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);

    if (ext == "ply")
    {
        //PLY mesh
        readPLY(*this, filename);
    }
    else
    {
        //Read with Surface_mesh module (OBJ, STL, OFF)
        read(filename);
    }

    update_face_normals();
    update_vertex_normals();

    // vertex properties
    auto vpositions = get_vertex_property<Vector3f>("v:point");
    assert(vpositions);

    auto vnormals = get_vertex_property<Vector3f>("v:normal");
    assert(vnormals);

    auto texcoords = get_vertex_property<Vector2f>("v:texcoord");
    if(!texcoords)
    {
      texcoords = add_vertex_property<Vector2f>("v:texcoord");
      this->texcoords().setZero();
    }

    auto colors = get_vertex_property<Vector3f>("v:color");
    if(!colors)
    {
      colors = add_vertex_property<Vector3f>("v:color");
      this->colors().setOnes();
    }

    add_vertex_property<int>("v:mask");
    this->masks().fill(0);

    auto vcurvatures = add_vertex_property<float>("v:kappa");
    auto vmaxcurv = add_vertex_property<float>("v:k1");
    auto vmincurv = add_vertex_property<float>("v:k2");
    auto vmaxcurvdir = add_vertex_property<Vector3f>("v:k1dir");
    auto vmincurvdir = add_vertex_property<Vector3f>("v:k2dir");
    auto vtrust = add_vertex_property<float>("v:trust");

    for(auto v:vertices()) {
      vcurvatures[v] = 0;
      vmaxcurv[v] = 0;
      vmincurv[v] = 0;
      vmaxcurvdir[v] = Vector3f::Zero();
      vmincurvdir[v] = Vector3f::Zero();
      vtrust[v] = 0;
    }

    // face iterator
    Surface_mesh::Face_iterator fit, fend = faces_end();
    // vertex circulator
    Surface_mesh::Vertex_around_face_circulator fvit, fvend;
    Surface_mesh::Vertex v0, v1, v2;
    for (fit = faces_begin(); fit != fend; ++fit)
    {
        fvit = fvend = vertices(*fit);
        v0 = *fvit;
        ++fvit;
        v2 = *fvit;

        do{
            v1 = v2;
            ++fvit;
            v2 = *fvit;
            _indices.push_back(v0.idx());
            _indices.push_back(v1.idx());
            _indices.push_back(v2.idx());
        } while (++fvit != fvend);
    }

    // Properties registration

    register_property<Vector3f>("v:point", "vtx_position", Mask::VERT_COORD);
    register_property<Vector3f>("v:normal", "vtx_normal", Mask::VERT_NORMAL);
    register_property<Vector3f>("v:color", "vtx_color", Mask::VERT_COLOR);
    register_property<Vector2f>("v:texcoord", "vtx_texcoord", Mask::VERT_TEXCOORD);
    register_property<int>("v:mask", "vtx_mask", Mask::VERT_MASK);
    register_property<float>("v:kappa", "vtx_kappa", Mask::VERT_KAPPA);
    register_property<float>("v:k1", "vtx_k1", Mask::VERT_K1);
    register_property<float>("v:k2", "vtx_k2", Mask::VERT_K2);
    register_property<Vector3f>("v:k1dir", "vtx_k1_dir", Mask::VERT_K1_DIR);
    register_property<Vector3f>("v:k2dir", "vtx_k2_dir", Mask::VERT_K2_DIR);
    register_property<float>("v:trust", "vtx_trust", Mask::VERT_TRUST);

    updateNormals();
    updateBoundingBox();
    updateBVH();
}





void Mesh:: createGrid(int m, int n)
{
  clear();
  _indices.clear();
  _indices.reserve(2*3*m*n);
  reserve(m*n, 3*m*n, 2*m*n);

  float dx = 1./float(m-1);
  float dy = 1./float(n-1);

  Eigen::Array<Surface_mesh::Vertex,Dynamic,Dynamic> ids(m,n);
  for(int i=0;i<m;++i)
    for(int j=0;j<n;++j)
      ids(i,j) = add_vertex(Point(double(i)*dx, double(j)*dy, 0.));

  add_vertex_property<Vector3f>("v:color");
  add_vertex_property<Vector3f>("v:normal");
  add_vertex_property<Vector2f>("v:texcoord");
  add_vertex_property<int>("v:mask");

  colors().fill(1);
  masks().fill(0);
  texcoords() = positions().topRows(2);

  for(int i=0; i<m-1; ++i) {
    for(int j=0; j<n-1; ++j) {
      Surface_mesh::Vertex v0,v1,v2,v3;
      v0 = ids(i+0,j+0);
      v1 = ids(i+1,j+0);
      v2 = ids(i+1,j+1);
      v3 = ids(i+0,j+1);
      add_triangle(v0,v1,v2);
      add_triangle(v0,v2,v3);

      _indices.push_back(v0.idx());
      _indices.push_back(v1.idx());
      _indices.push_back(v2.idx());

      _indices.push_back(v0.idx());
      _indices.push_back(v2.idx());
      _indices.push_back(v3.idx());
    }
  }
  updateNormals();
  updateBoundingBox();
  updateBVH();
}

void Mesh::init()
{
    glGenVertexArrays(1, &_vao);

    // Buffers generation
    glGenBuffers(1, &_faces_vbo_id);
    for (auto& attr : _attributeProperties)
        glGenBuffers(1, &attr.vbo_id);

    updateVBO(Mask::ALL);
    _ready = true;
}

void Mesh::updateAll()
{
  updateNormals();
  updateBoundingBox();
  updateBVH();
  updateVBO(Mask::ALL);
}

void Mesh::updateNormals()
{
  update_face_normals();
  update_vertex_normals();
}

void Mesh::updateVBO(int vbo_to_update)
{
  glBindVertexArray(_vao);

  if (vbo_to_update & Mask::FACE_INDEX) {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _faces_vbo_id);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * _indices.size(), _indices.data(),  GL_STATIC_DRAW);
  }

  // Loop on mesh registered properties
  for (auto& attr : _attributeProperties)
  {
    if(attr.size > 0 && attr.mask & vbo_to_update) {
      glBindBuffer(GL_ARRAY_BUFFER, attr.vbo_id);
      glBufferData(GL_ARRAY_BUFFER, attr.nb_bytes_per_element*attr.size, attr.data, GL_STATIC_DRAW);
    }
  }

  glBindVertexArray(0);
}

void Mesh::updateBoundingBox()
{
  _bbox.setNull();
  Surface_mesh::Vertex_property<Vector3f> vertices = get_vertex_property<Vector3f>("v:point");
  for(const auto& p : vertices.vector())
      _bbox.extend(p);
}

void Mesh::updateBVH()
{
    if(_bvh)
        delete _bvh;
    _bvh = new BVH;
    _bvh->build(this, 10, 100);
}

void Mesh::draw(const Shader &shader)
{
    if (!_ready)
        init();

    glBindVertexArray(_vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _faces_vbo_id);

    std::stack<int> attr_locations;

    for (auto& attr : _attributeProperties) {
        int attr_loc = shader.getAttribLocation(attr.gl_name.c_str());

        if (attr_loc >= 0) {
            glBindBuffer(GL_ARRAY_BUFFER, attr.vbo_id);
            glVertexAttribPointer(attr_loc, attr.nb_coeffs_per_element, attr.gl_scalar_type, GL_FALSE, 0, (void*)0);
            glEnableVertexAttribArray(attr_loc);

            attr_locations.push(attr_loc);
        }
    }

    glDrawElements(GL_TRIANGLES, _indices.size(),  GL_UNSIGNED_INT, 0);

    while (!attr_locations.empty()) {
        glDisableVertexAttribArray(attr_locations.top());
        attr_locations.pop();
    }

    glBindVertexArray(0);
}


bool Mesh::intersectFace(const Ray& ray, Hit& hit, int faceId) const
{
  auto vertices = get_vertex_property<Vector3f>("v:point").vector();
  Vector3f v0 = vertices[_indices[faceId*3+0]];
  Vector3f v1 = vertices[_indices[faceId*3+1]];
  Vector3f v2 = vertices[_indices[faceId*3+2]];
  Vector3f e1 = v1 - v0;
  Vector3f e2 = v2 - v0;
  Eigen::Matrix3f M;
  M << -ray.direction, e1, e2;
  Vector3f tuv = M.inverse() * (ray.origin - v0);
  float t = tuv(0), u = tuv(1), v = tuv(2);
  if(t>0 && u>=0 && v>=0 && (u+v)<=1 && t<hit.t())
  {
      hit.setT(t);

      hit.setFaceId(faceId);
      hit.setBaryCoords(Vector3f(1.-u-v,u,v));

      return true;
  }
  return false;
}

float& Mesh::alpha()
{
    return _alpha;
}

float& Mesh::alpha_bar()
{
    return _alpha_bar;
}

float& Mesh::beta()
{
    return _beta;
}

float& Mesh::beta_bar()
{
    return _beta_bar;
}

void Mesh::setAlpha(float alpha)
{
    _alpha = alpha;
}

void Mesh::setAlpha_bar(float alpha_bar)
{
    _alpha_bar = alpha_bar;
}

void Mesh::setBeta(float beta)
{
    _beta = beta;
}

void Mesh::setBeta_bar(float beta_bar)
{
    _beta_bar = beta_bar;
}

static float clamp(float x, float a, float b)
{
    if (x < a)
        return a;
  if (x > b)
    return b;

  return x;
}

void Mesh::fit(float t)
{
    typedef DistWeightFunc< Sample, SmoothWeightKernel<Sample::Scalar> > WeightFunc;
    typedef Basket<Sample, WeightFunc, OrientedSphereFit, GLSParam, OrientedSphereSpaceDer, GLSDer, GLSCurvatureHelper> SphericalFit;

    auto vkappa = get_vertex_property<float>("v:kappa");
    auto vk1 = get_vertex_property<float>("v:k1");
    auto vk2 = get_vertex_property<float>("v:k2");
    auto vk1dir = get_vertex_property<Vector3f>("v:k1dir");
    auto vk2dir = get_vertex_property<Vector3f>("v:k2dir");
    auto vtrust = get_vertex_property<float>("v:trust");

    auto masks = get_vertex_property<int>("v:mask");

    std::fill(vkappa.vector().begin(), vkappa.vector().end(), 0);
    std::fill(vk1dir.vector().begin(), vk1dir.vector().end(), Vector3f::Zero());
    std::fill(vk2dir.vector().begin(), vk2dir.vector().end(), Vector3f::Zero());

    #pragma omp parallel
    {
        // Create the previously defined fitting procedure
        SphericalFit fit;
        // Set a weighting function instance
        fit.setWeightFunc(WeightFunc(t));

        #pragma omp for
        // Loop over selected vertices
        for (int i = 0; i < _selectedVertices.size(); ++i) {
            Vertex vert(_selectedVertices[i]);
            fit.init(position(vert));
            int np = 0;
            do {
                foreach_neighbors(*this, vert, t, [&] (Vertex nei) { fit.addNeighbor(vertexToSample(nei)); });
                ++np;
            } while(fit.finalize()==NEED_OTHER_PASS);
            if(np>1 && vert==_selectedVertices[0])
                std::cout << "WARNING, required " << np << " passes!!\n";

            // Get mean curvature
            vkappa[vert] = (float) fit.kappa();

            // Get principal curvatures indices and directions
            vk1[vert] = (float) fit.GLSk1();
            vk2[vert] = (float) fit.GLSk2();
            vtrust[vert] = clamp(1 - (std::abs(vk2[vert]) - 1./(_beta * t))/(1./(_beta_bar * t) - 1./(_beta * t)), 0.0, 1.0);
            vtrust[vert] *= clamp((-vk1[vert] - 1./(_alpha * t))/(1./(_alpha_bar * t) - 1./(_alpha * t)), 0.0, 1.0);
            vk1dir[vert] = fit.GLSk1Direction(); vk1dir[vert].normalize();
            vk2dir[vert] = fit.GLSk2Direction(); vk2dir[vert].normalize();

//            std::cout << "Potiential of point " << position(vert).transpose() << " : " << fit.potential(/*position(vert)*/) << std::endl;
        }
    }

    // Compute minimum and maximum curvature values
    auto minmax_curv = std::minmax_element(vkappa.vector().begin(), vkappa.vector().end());
    auto minmax_k1 = std::minmax_element(vk1.vector().begin(), vk1.vector().end());
    auto minmax_k2 = std::minmax_element(vk2.vector().begin(), vk2.vector().end());
    float min_curv = *minmax_curv.first;
    float max_curv = *minmax_curv.second;
    float min_k1 = *minmax_k1.first;
    float max_k1 = *minmax_k1.second;
    float min_k2 = *minmax_k2.first;
    float max_k2 = *minmax_k2.second;

    if (-min_curv > max_curv) max_curv = -min_curv;
    if (-min_curv < max_curv) min_curv = -max_curv;
    if (-min_k1 > max_k1) max_k1 = -min_k1;
    if (-min_k1 < max_k1) min_k1 = -max_k1;
    if (-min_k2 > max_k2) max_k2 = -min_k2;
    if (-min_k2 < max_k2) min_k2 = -max_k2;

    // Normalize curvatures depending on extrema
    for (auto vit = vertices_begin(); vit != vertices_end(); ++vit) {
        vkappa[*vit] = vkappa[*vit] / (2*max_curv);
        vk1[*vit] /= (2*max_k1);
        vk2[*vit] /= (2*max_k2);
    }
}

std::vector< std::pair<float, int> > Mesh::histogram(float min, float max, int ninter, std::vector<float> values)
{
  float step = (max - min) / (float) ninter;
  std::vector< std::pair<float, int> > hist(ninter);
  std::fill(hist.begin(), hist.end(), std::make_pair(0.0, 0));
  hist[0].first = min;

  for (int i = 1; i < hist.size(); ++i)
  {
    hist[i].first = hist[i - 1].first + step;
  }

  for (auto val : values)
  {
    int index = (int) ((val - min)/step);
    ++hist[index].second;
  }

  return hist;
}

std::string Mesh::replace(const char* str, const char* from, const char* to)
{
    std::string sstr(str);
    std::string sfrom(from);
    std::string sto(to);

    size_t pos = sstr.find(sfrom);
    if (pos == std::string::npos)
        return nullptr;
    sstr.replace(pos, sfrom.length(), sto);

    return sstr;
}

void Mesh::updateSelection()
{
    auto vmask = get_vertex_property<int>("v:mask");
    _selectedVertices.clear();

    for (auto vit = vertices_begin(); vit != vertices_end(); ++vit)
        if (vmask[*vit]) _selectedVertices.emplace_back(*vit);
}

void Mesh::computeScaleWeights(float t)
{
    typedef DistWeightFunc< Sample, SmoothWeightKernel<Sample::Scalar> > WeightFunc;
    typedef Basket<Sample, WeightFunc, OrientedSphereFit, GLSParam, OrientedSphereSpaceDer, GLSDer, GLSCurvatureHelper> OrientedSphereFit;

    auto vk1 = get_vertex_property<float>("v:k1");
    auto vk2 = get_vertex_property<float>("v:k2");

    auto masks = get_vertex_property<int>("v:mask");

    std::fill(vk1.vector().begin(), vk1.vector().end(), 0);
    std::fill(vk2.vector().begin(), vk2.vector().end(), 0);

    #pragma omp parallel
    {
        // Create the previously defined fitting procedure
        OrientedSphereFit fit;
        // Set a weighting function instance
        fit.setWeightFunc(WeightFunc(t));

        #pragma omp for
        // Loop over selected vertices
        for (int i = 0; i < _selectedVertices.size(); ++i) {
            Vertex vert(_selectedVertices[i]);
            fit.init(position(vert));
            int np = 0;
            do {
                foreach_neighbors(*this, vert, t, [&] (Vertex nei) { fit.addNeighbor(vertexToSample(nei)); });
                ++np;
            } while(fit.finalize()==NEED_OTHER_PASS);
            if(np>1 && vert==_selectedVertices[0])
                std::cout << "WARNING, required " << np << " passes!!\n";

            // Get principal curvatures indices
            vk1[vert] = (float) fit.GLSk1();
            vk2[vert] = (float) fit.GLSk2();
        }
    }

    std::vector<float> h1k1;
    std::vector<float> h1k2;

    for (auto vert : _selectedVertices) {
      h1k1.emplace_back(-t * vk1[vert]);
      h1k2.emplace_back(t * std::abs(vk2[vert]));
    }

    auto minmax_h1k1 = std::minmax_element(h1k1.begin(), h1k1.end());
    auto minmax_h1k2 = std::minmax_element(h1k2.begin(), h1k2.end());

    float min_h1k1 = *minmax_h1k1.first;
    float max_h1k1 = *minmax_h1k1.second;
    float min_h1k2 = *minmax_h1k2.first;
    float max_h1k2 = *minmax_h1k2.second;


    _histh1k1 = histogram(min_h1k1, max_h1k1, 100, h1k1);
    _histh1k2 = histogram(min_h1k2, max_h1k2, 100, h1k2);

//    for (auto elem : _histh1k1)
//        std::cout << elem.first << " : " << elem.second << std::endl;
//    std::cout << std::endl;
//    for (auto elem : _histh1k2)
//        std::cout << elem.first << " : " << elem.second << std::endl;

    // Determine alpha and beta weights

    _alpha = 1./min_h1k1;
    _alpha_bar = 1./max_h1k1;
    _beta = 1./min_h1k2;
    _beta_bar = 1./max_h1k2;

    std::cout << "beta : " << _beta << std::endl;
    std::cout << "beta_bar : " << _beta_bar << std::endl;
    std::cout << "alpha : " << _alpha << std::endl;
    std::cout << "alpha_bar : " << _alpha_bar << std::endl;
}

void Mesh::passTwo(float t)
{
  typedef DistWeightFunc< Sample, SmoothWeightKernel<Sample::Scalar> > WeightFunc;
  typedef Basket<Sample, WeightFunc, CompactPlane, CovariancePlaneFit> CovPlaneFit;
  typedef Basket<Sample, WeightFunc, UnorientedSphereFit, GLSParam> UnorientedSphereFit;
  float gamma = 4.;
  float trust_ref = 0.5;
  float h2 = gamma*t;
  std::vector<Sample> projectedNeighbors;

  auto vtrust = get_vertex_property<float>("v:trust");
  auto vcolor = get_vertex_property<Vector3f>("v:color");
  auto vk2dir = get_vertex_property<Vector3f>("v:k2dir");

  #pragma omp parallel
  {

    CovPlaneFit planeFit;
    planeFit.setWeightFunc(WeightFunc(h2));
    UnorientedSphereFit sphereFit;

    int n = 0;

    #pragma omp for
    for (int i = 0; i < _selectedVertices.size(); ++i)
    {
      Vertex vert(_selectedVertices[i]);
    //    if (vtrust[vert] > trust_ref)
    //    {

      /// Planar Fit

      planeFit.init(position(vert));
      int np = 0;
      do {
        ++np;
        foreach_neighbors(*this, vert, h2, [&] (Vertex nei) { planeFit.addNeighbor(vertexToSample(nei)); });
      } while (planeFit.finalize() == NEED_OTHER_PASS);
      if (np > 1 && vert == _selectedVertices[0])
        std::cout << "Warning : planar fit required " << np << " passes !" << std::endl;

      if (planeFit.isStable())
      {
        /// Spherical fit

        Sample::VectorType projVert = planeFit.project(position(vert));
        sphereFit.init(projVert);
        sphereFit.setWeightFunc(vtrust[vert]);

        np = 0;
        do {
          ++np;
          foreach_neighbors(*this, vert, h2, [&] (Vertex nei) {
              Vector3f normal = planeFit.project(vk2dir[nei]).unitOrthogonal(); // Consider the normal as the orthogonal vector to the projection of the minimal curvature direction onto the fitted plane
//              std::cout << position(nei).transpose() << std::endl;
              Sample projectedNei{planeFit.project(position(nei)), normal};
              sphereFit.addNeighbor(projectedNei);
          });
//          std::cout << std::endl;
        } while (sphereFit.finalize() == NEED_OTHER_PASS);
        if (np > 1 && vert == _selectedVertices[0])
          std::cout << "Warning : sphere fit required " << np << " passes !" << std::endl;

//        foreach_neighbors(*this, vert)
//        Scalar diff = (1.0 + sphereFit.potential(projVert) * 10e3) * (1./2.);
        Scalar diff = std::abs(sphereFit.potential()) * 10e2;
        vcolor[vert] = Vector3f(diff, diff, diff);
        if (diff>1.) std::cout << diff << std::endl;

//        if (!isnan(vcolor[vert].x()) && vcolor[vert].x() > 10e-3)
//        {
    //          std::cout << "Sphere center : " << sphereFit.center().transpose() << std::endl;
    //          std::cout << "Sphere radius : " << sphereFit.radius() << std::endl;
    //          std::cout << "Projection : " << projVert.transpose() << std::endl;
    //          std::cout << "Diff vector : " << v.transpose() << std::endl;
    //          std::cout << "Diff vector norm: " << v.norm() << std::endl;
//          std::cout << vcolor[vert].transpose() << std::endl;
//        }
      }
      else
      {
        std::cout << "Plane fit not stable !" << std::endl;
      }
    //    }
    }
  }
}


bool Mesh::intersect(const Ray& ray, Hit& hit) const
{
    if(_bvh)
    {
        // use the BVH !!
        return _bvh->intersect(ray, hit);
    }
    else
    {
        // brute force !!
        bool ret = false;
        float tMin, tMax;
        Normal3f normal;
        if( (!::intersect(ray, _bbox, tMin, tMax,normal)) || tMin>hit.t())
            return false;

        for(unsigned int i=0; i<n_faces(); ++i)
        {
            ret = ret | intersectFace(ray, hit, i);
        }
        return ret;
    }
}


template<typename T>
bool Mesh::hasProperty(const string name) const
{
    Surface_mesh::Vertex_property<T> p = get_vertex_property<T>(name.c_str());

    return p != NULL;
}

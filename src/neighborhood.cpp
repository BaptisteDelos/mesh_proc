
#include "mesh.h"
#include "neighborhood.h"
#include <vector>

using namespace surface_mesh;
using namespace Eigen;
using namespace std;

std::vector<Surface_mesh::Vertex> select_neighbors(const Surface_mesh& mesh, Surface_mesh::Vertex v, float dist)
{
  std::vector<Surface_mesh::Vertex> neighbors;
  foreach_neighbors(mesh,v,dist, [&] (Surface_mesh::Vertex v) {neighbors.push_back(v);});
  return neighbors;
}

/*
SurfaceMesh::Face f = ????;
for(auto v:mesh.vertices(f)) cog += mesh.position(v);

SurfaceMesh::Vertex vi = ????;
for(auto vj:mesh.vertices(vi)) cog += mesh.position(v);
*/

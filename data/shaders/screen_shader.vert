#version 330 core
layout (location = 0) in vec3 vpos;
layout (location = 1) in vec2 vtexcoord;

out vec2 vert_uv;

void main()
{
    gl_Position = vec4(vpos, 1.0);
    vert_uv = vtexcoord;
}

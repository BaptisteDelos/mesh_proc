#version 330 core

in vec3 vert_color;

layout(location = 0) out vec4 out_color;

void main(void)
{
  out_color = vec4(vert_color,1);
}

#version 330 core

//in vec2 vert_k1_dir;
in vec2 vert_k1_dir;
in vec2 vert_k2_dir;

//layout(location = 0) out vec2 out_curv;
out vec4 out_color;

void main(void)
{
//  out_curv = vert_k1_dir;
  //out_curv = sin(gl_FragCoord.xy/100)*0.5+vec2(0.5);
//  out_color = vec4(sin(gl_FragCoord.xy/50)*0.5+vec2(0.5),0,1);
  //out_color = vec4(10*normalize(vert_k1_dir)*0.5+vec3(0.5),1);
  out_color = vec4(vert_k2_dir,0,1);
//  out_curv = vert_k2_dir;
}

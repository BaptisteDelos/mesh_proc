#version 330 core

in vec2 texcoord;

uniform sampler2D screenTexture; // flow field
uniform sampler2D image;         // noise texture

out vec4 out_color;

vec2 pixel_size = 1./vec2(textureSize(image,0));
float scale = 1.;
float size = 10;

vec2 flow(in vec2 p) {
    vec2 f = texture2D(screenTexture,p).xy * scale;
    if (length(f) > 1.) return normalize(f);

    return f;
}

vec2 nextFlow(in vec2 p, in vec2 pf) {
    vec2 nf = flow(p);
    return dot(nf, pf) < 0. ? -nf : nf;
}

vec4 val(in vec2 p) {
    return texture2D(image, p);
}

vec4 lic(in vec2 p) {
    vec2 c1 = p;
    vec2 c2 = p;
    vec2 f1 = flow(c1);
    vec2 f2 = -f1;

    vec4 r = val(c1);
    float a = r.w;
    float s = 1.;
    
    for (int i = 0; i < size; ++i) {
	c1 += f1*pixel_size;
	c2 += f2*pixel_size;

	r += val(c1);
	r += val(c2);
	s += 2;

	f1 = nextFlow(c1, f1);
	f2 = nextFlow(c2, f2);
    }

    return vec4(r.xyz/s, a);
}

void main() {
    out_color = lic(texcoord);
}


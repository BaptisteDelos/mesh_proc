#version 330 core

uniform mat4 proj_mat;
uniform mat4 view_mat;
uniform mat4 obj_mat;
uniform int disp_selection;

in vec3 vtx_position;
in int vtx_mask;
in float vtx_kappa;

out vec3 vert_color;

void main()
{
  gl_Position = proj_mat * view_mat * obj_mat * vec4(vtx_position,1.0);
  mat3 ref_colors;
  ref_colors[0] = vec3(1, 0, 0);
  ref_colors[1] = vec3(1, 1, 1);
  ref_colors[2] = vec3(0, 0, 1);

  if(bool(disp_selection) && (vtx_mask!=0)) {
    if (vtx_kappa < 0.)
      vert_color = -vtx_kappa * ref_colors[0] + (1. + vtx_kappa) * ref_colors[1];
    else
      vert_color = (1. - vtx_kappa) * ref_colors[1] + vtx_kappa * ref_colors[2];
  } else
    vert_color = vec3(0,0,0);
}

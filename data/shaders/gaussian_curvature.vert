#version 330 core

uniform mat4 proj_mat;
uniform mat4 view_mat;
uniform mat4 obj_mat;
uniform int disp_selection;

in vec3 vtx_position;
in int vtx_mask;
in float vtx_k1;
in float vtx_k2;

out vec3 vert_color;

void main()
{
  gl_Position = proj_mat * view_mat * obj_mat * vec4(vtx_position,1.0);
  mat3 ref_colors;
  ref_colors[0] = vec3(1, 0, 0);
  ref_colors[1] = vec3(1, 1, 1);
  ref_colors[2] = vec3(0, 0, 1);

  float sign_k1 = vtx_k1 >= 0 ? 1.0 : -1.0;
  float sign_k2 = vtx_k2 >= 0 ? 1.0 : -1.0;

  float gauss_curv = sign_k1 * sign_k2 * exp((log2(sign_k1 * vtx_k1) + log2(sign_k2 * vtx_k2))/exp(1.0));

  if(bool(disp_selection) && (vtx_mask!=0)) {
    if (gauss_curv < 0.)
      vert_color = -gauss_curv * ref_colors[0] + (1. + gauss_curv) * ref_colors[1];
    else
      vert_color = (1. - gauss_curv) * ref_colors[1] + gauss_curv * ref_colors[2];
  } else
    vert_color = vec3(0,0,0);
}

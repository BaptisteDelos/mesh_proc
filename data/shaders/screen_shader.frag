
#version 330 core

in vec2 vert_uv;

out vec4 out_color;

uniform sampler2D screenTexture;

void main()
{
    out_color = vec4(texture2D(screenTexture, vert_uv).xyz,1);
}

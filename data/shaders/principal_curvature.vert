#version 330 core

uniform mat4 proj_mat;
uniform mat4 view_mat;
uniform mat4 obj_mat;

in vec3 vtx_position;
in vec3 vtx_k1_dir;
in vec3 vtx_k2_dir;

//out vec3 vert_k1_dir;
out vec2 vert_k1_dir;
//out vec3 vert_k2_dir;
out vec2 vert_k2_dir;

void main()
{
  gl_Position = proj_mat * view_mat * obj_mat * vec4(vtx_position,1.0);

  vec4 k1 = proj_mat * view_mat * obj_mat * vec4(vtx_k1_dir, 1.0);
  vec4 k2 = proj_mat * view_mat * obj_mat * vec4(vtx_k2_dir, 1.0);

  vert_k1_dir = /*vtx_k1_dir;*/k1.xy;
  vert_k2_dir = /*vtx_k2_dir;*/k2.xy;
}

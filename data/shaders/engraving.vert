#version 330 core

uniform mat4 proj_mat;
uniform mat4 view_mat;
uniform mat4 obj_mat;
uniform mat3 normal_mat;
uniform int disp_selection;

in vec3 vtx_position;
in vec3 vtx_color;
in int vtx_mask;

out vec3 vert_color;

void main()
{
  gl_Position = proj_mat * view_mat * obj_mat * vec4(vtx_position,1.0);

  if(bool(disp_selection) && (vtx_mask!=0))
    vert_color = vtx_color;
  else
    vert_color = vec3(0, 0, 0);
}
